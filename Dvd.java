public class Dvd extends Item
{
private String director;

public Dvd (String title, int time, String director){
    super(title, time);
    this.director = director;
}
}